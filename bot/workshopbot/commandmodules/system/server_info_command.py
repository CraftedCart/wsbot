import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class ServerInfoCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        # Check if we're in a DM or something
        if message.guild is None:
            em = discord.Embed(
                title="⚠️ Cannot get server info when not in a server",
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        em = discord.Embed(
            title="ℹ️ Server info for %s" % message.guild.name,
            description="**ID**: %s\n" % message.guild.id +
                        "**Region**: %s\n" % message.guild.region +
                        "**Creation date**: %s UTC\n" % message.guild.created_at,
            color=0x2196F3
        )
        await message.channel.send(embed=em)

    @classmethod
    def get_command_name(cls):
        return "server-info"

    @classmethod
    def get_summary_text(cls):
        return "Retrieves information about the server the bot was messaged in"

    @classmethod
    def get_help_text(cls):
        return "Retrieves information about the server the bot was messaged in\n" \
               "**Usage**: %s" % cls.get_command_name()
