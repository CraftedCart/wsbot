import os
import platform
import time

import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class BotInfoCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        em = discord.Embed(
            title="ℹ️"
                  " Bot info for %s" % bot.user.name,
            description="**Platform**: %s %s\n" % (platform.system(), platform.machine()) +
                        "**Hostname**: %s\n" % platform.node() +
                        "**Bot uptime**: %s\n" % time.strftime('%H:%M:%S', time.gmtime(time.time() - bot.init_time)) +
                        "**Bot PID**: %d\n" % os.getpid(),
            color=0x2196F3
        )
        await message.channel.send(embed=em)

    @classmethod
    def get_command_name(cls):
        return "bot-info"

    @classmethod
    def get_summary_text(cls):
        return "Retrieves information about this bot"

    @classmethod
    def get_help_text(cls):
        return "Retrieves information about this bot\n" \
               "**Usage**: %s" % cls.get_command_name()
