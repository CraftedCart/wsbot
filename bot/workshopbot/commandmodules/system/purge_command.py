import asyncio
import logging
from typing import List

import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class PurgeCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        if user_cmd.argc < 2:
            em = discord.Embed(
                title="⚠️ Invalid number of arguments",
                description="Usage: %s count" % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        try:
            count = int(user_cmd.argv[1]) + 1
        except ValueError:
            em = discord.Embed(
                title="⚠️ Not an integer",
                description="Usage: %s count" % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        if count <= 0:
            em = discord.Embed(
                title="⚠️ Can't delete a negative number of messages",
                description="Usage: %s count" % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        async for msg in message.channel.history(limit=count, reverse=True):
            end_message = msg
            break

        await message.channel.send("__**Delete until the below message?**__\n\n" + end_message.content)

        reply_prefix = bot.command_prefix + bot.command_prefix

        def reply_check(msg: discord.Message):
            return msg.content.startswith(bot.command_prefix) and \
                   msg.author == message.author and \
                   msg.channel == message.channel

        listen_for_replies = True

        while listen_for_replies:
            try:
                reply_message: discord.Message = await bot.wait_for(
                    "message",
                    timeout=120,
                    check=reply_check
                )

                if reply_message is not None:
                    # If we don't start with the reply prefix, but do start with the regular prefix, stop listening for
                    # replies
                    if reply_message.content.startswith(reply_prefix):
                        try:
                            await reply_message.delete()
                        except discord.Forbidden:
                            logging.info("Failed to delete reply message")

                        if reply_message.content.startswith(reply_prefix + "y"):  # Yes
                            await message.channel.purge(limit=count + 1)

                        elif reply_message.content.startswith(reply_prefix + "n"):  # No
                            listen_for_replies = False

                    else:
                        listen_for_replies = False

            except asyncio.TimeoutError:
                pass  # TODO

    @classmethod
    def get_command_name(cls):
        return "purge"

    @classmethod
    def get_summary_text(cls):
        return "Delete many messages"

    @classmethod
    def get_help_text(cls):
        return "Delete many messages\n" \
               "**Usage**: %s count" % cls.get_command_name()

    # Temporary
    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
