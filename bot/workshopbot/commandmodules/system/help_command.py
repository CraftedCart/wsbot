import discord

import workshopbot
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class HelpCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        out_str = ""
        if user_cmd.argc > 1:
            for i in range(1, user_cmd.argc):
                if user_cmd.argv[i] in workshopbot.bot.registered_commands:
                    out_str += "__**%s**__\n%s\n\n" % (
                        user_cmd.argv[i],
                        workshopbot.bot.registered_commands[user_cmd.argv[i]].get_help_text()
                    )
                else:
                    out_str += "__**%s**__\nNot a command\n\n" % user_cmd.argv[i]
        else:
            out_str = "__**Commands**__\n\n"

            for name, cmd in workshopbot.bot.registered_commands.items():
                out_str += "**%s**: %s\n" % (name, cmd.get_summary_text())

        await message.channel.send(out_str)

    @classmethod
    def get_command_name(cls):
        return "help"

    @classmethod
    def get_summary_text(cls):
        return "Shows this help text - You can also do `help [command...]` to get info on specific commands"

    @classmethod
    def get_help_text(cls):
        return "**Usage**: help [command...]"
