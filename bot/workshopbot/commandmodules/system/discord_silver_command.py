import os
import platform
import time

import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class BotInfoCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        await message.channel.send("%s has been awarded Discord Silver\nhttps://i.imgur.com/Vt7NLdA.png" % user_cmd.argv[1])

    @classmethod
    def get_command_name(cls):
        return "discord-silver"

    @classmethod
    def get_summary_text(cls):
        return "\\o/"

    @classmethod
    def get_help_text(cls):
        return "\\o/\n" \
               "**Usage**: %s" % cls.get_command_name()
