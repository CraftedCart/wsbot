from .error_command import *
from .reboot_command import *
from .reload_commands_command import *
from .reload_bot_modules_command import *
from .test_command import *

command_modules = [
    error_command,
    reboot_command,
    reload_commands_command,
    reload_bot_modules_command,
    test_command
]
