import importlib
import sys
from typing import List

import discord

import workshopbot
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class ReloadCommandsCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        prev_command_count = len(workshopbot.bot.registered_commands)
        workshopbot.bot.registered_commands.clear()

        importlib.reload(workshopbot.commands)
        importlib.reload(workshopbot.commandmodules)

        for commandmodule in workshopbot.commandmodules.commandmodule_modules:
            if commandmodule.__name__ in sys.modules:
                importlib.reload(commandmodule)
            else:
                import commandmodule

            for module in commandmodule.command_modules:
                importlib.reload(module)

        new_command_count = len(workshopbot.bot.registered_commands)
        await message.channel.send(
            "Unregistered %d commands, registered %d commands" % (prev_command_count, new_command_count)
        )

    @classmethod
    def get_command_name(cls):
        return "reload-commands"

    @classmethod
    def get_summary_text(cls):
        return "Unloads and reloads all command modules"

    @classmethod
    def get_help_text(cls):
        return "Unloads and reloads all command modules\n" \
               "**Usage**: %s" % cls.get_command_name()

    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
