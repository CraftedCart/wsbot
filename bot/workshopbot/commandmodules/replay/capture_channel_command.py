import json
import logging
import os
from typing import List

import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class CaptureChannelCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        if user_cmd.argc < 2:
            em = discord.Embed(
                title="⚠️ Invalid number of arguments",
                description="Usage: %s #channel" % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        if len(message.channel_mentions) < 1:
            em = discord.Embed(
                title="⚠️ Must mention a channel",
                description="Usage: %s #channel" % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        em = discord.Embed(
            title="Capturing channel history for #%s" % message.channel_mentions[0].name,
            description="0 messages captured, 0 attachments saved\n...",
            color=0x2196F3
        )
        status_msg = await message.channel.send(embed=em)

        os.makedirs(f"capture-{message.channel_mentions[0].name}/attachments", exist_ok=True)

        messages = []
        i = 0
        attachments = 0
        async for msg in message.channel_mentions[0].history(limit=None):
            messages.insert(0, {
                "author": {
                    "name": msg.author.name,
                    "discriminator": msg.author.discriminator,
                    "display_name": msg.author.display_name,
                    "bot": msg.author.bot,
                    "id": msg.author.id
                },
                "content": msg.content,
                "clean_content": msg.clean_content,
                "pinned": msg.pinned,
                "attachments": [f"{att.id}_{att.filename}" for att in msg.attachments],
                "id": msg.id
            })

            for att in msg.attachments:
                with open(f"capture-{message.channel_mentions[0].name}/attachments/{att.id}_{att.filename}", "wb") as f:
                    await att.save(f)

                attachments += 1

            i += 1
            if i % 1000 == 0:
                em.description = "%d messages captured, %d attachments saved\nAt time %s UTC" % \
                                 (i, attachments, msg.created_at)
                await status_msg.edit(embed=em)

        with open(f"capture-{message.channel_mentions[0].name}/history.json", "w") as f:
            f.write(json.dumps(messages))

        em.title = "Finished capturing channel history for #%s" % message.channel_mentions[0].name
        em.description = "%d messages captured, %d attachments saved\nSaved to %s" % \
                         (
                             i,
                             attachments,
                             f"{os.path.join(os.getcwd(), 'capture-' + message.channel_mentions[0].name)}/"
                         )
        await status_msg.edit(embed=em)

    @classmethod
    def get_command_name(cls):
        return "capture-channel"

    @classmethod
    def get_summary_text(cls):
        return "Archives an entire channel's chat history"

    @classmethod
    def get_help_text(cls):
        return "Archives an entire channel's chat history\n" \
               "**Usage**: %s #channel" % cls.get_command_name()

    # Temporary
    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
