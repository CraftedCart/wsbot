from typing import List

import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class NotifyMeCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        # Check if we're in a DM or something
        if message.guild is None:
            em = discord.Embed(
                title="⚠️ Cannot notify you outside of a server",
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        if user_cmd.argc != 2:
            em = discord.Embed(
                title="⚠️ Invalid number of arguments",
                description="Usage: %s name" % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        # TODO Make this not hardcoded
        if user_cmd.argv[1] == "leveljams":
            role = discord.utils.get(message.guild.roles, name="Level Jam Announcements")
            if role in message.author.roles:
                await message.author.remove_roles(role)
                em = discord.Embed(
                    title="🔔 You will no longer be notified for level jam events",
                    description="Removed `%s` role" % role.name,
                    color=0x2196F3
                )
            else:
                await message.author.add_roles(role)
                em = discord.Embed(
                    title="🔔 You will now be notified for level jam events",
                    description="Added `%s` role" % role.name,
                    color=0x2196F3
                )

            await message.channel.send(embed=em)
        else:
            em = discord.Embed(
                title="⚠️ Invalid event-name",
                description="Usage: %s event-name" % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)

    @classmethod
    def get_command_name(cls):
        return "notify-me"

    @classmethod
    def get_summary_text(cls):
        return "Be notified about server events"

    @classmethod
    def get_help_text(cls):
        return "Be notified about server events\n" \
               "**Usage**: %s event-name" % cls.get_command_name()
