from .add_server_group_command import *
from .del_server_group_command import *
from .add_server_group_users_command import *
from .get_user_groups_command import *
from .list_groups_command import *
from .notify_me_command import *

command_modules = [
    add_server_group_command,
    del_server_group_command,
    add_server_group_users_command,
    get_user_groups_command,
    list_groups_command,
    notify_me_command
]
