import discord

from workshopbot import user_permissions
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class GetUserGroupsCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        if user_cmd.argc >= 2:
            if len(message.mentions) > 0:
                user = message.mentions[0]
            else:
                # There was an argv[1] but nobody was mentioned
                em = discord.Embed(
                    title="⚠️ No user was mentioned but a parameter was given",
                    description="Run the command without the parameter or mention a user",
                    color=0xF44336
                )
                await message.channel.send(embed=em)
                return
        else:
            user = message.author

        em = discord.Embed(
            title="🔐 Permission groups for %s" % user.name,
            color=0x2196F3
        )

        # Fetch and add global groups
        global_groups = user_permissions.get_user_global_permissions(int(user.id))
        global_groups_str = ""
        for group in global_groups:
            global_groups_str += "%s `%s`\n" % (group.permission_name, group.permission_uuid)
        if global_groups_str == "":
            global_groups_str = "None"
        em.add_field(name="Global groups", value=global_groups_str, inline=False)

        # Fetch and add guild groups
        if message.guild is not None:
            guild_groups = user_permissions.get_user_guild_permissions(int(user.id), int(message.guild.id))
            guild_groups_str = ""
            for group in guild_groups:
                guild_groups_str += "%s `%s`\n" % (group.permission_name, group.permission_uuid)
            if guild_groups_str == "":
                guild_groups_str = "None"
            em.add_field(name="%s groups" % message.guild.name, value=guild_groups_str, inline=False)

        await message.channel.send(embed=em)

    @classmethod
    def get_command_name(cls):
        return "get-user-groups"

    @classmethod
    def get_summary_text(cls):
        return "Gets a list of permission groups you're in"

    @classmethod
    def get_help_text(cls):
        return "Gets a list of permission groups you're in\n" \
               "**Usage**: %s [user]" % cls.get_command_name()
