import asyncio
import logging
from typing import List

import discord

from workshopbot.smb import smb_db
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand
from workshopbot.smb.smb_stage import SmbStage


@bot_command
class SmbFindStageCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        if user_cmd.argc < 2:
            em = discord.Embed(
                title="⚠️ Invalid number of arguments",
                description="Usage: %s name..." % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        name_args = user_cmd.argv
        name_args.pop(0)  # Remove the command

        stages = smb_db.find_stages_by_name(" ".join(name_args))

        # TODO: Pagination or something
        reply_prefix = bot.command_prefix + bot.command_prefix

        em = discord.Embed(
            title="🔍 Found %d stages" % len(stages),
            description="Replies time out after 2m\n" +
                        "Reply with `%sn` / `%sp` for the next/previous page\n" % (reply_prefix, reply_prefix) +
                        "Reply with `%s4` to view more info about the 4th entry" % (reply_prefix,),
            color=0x4CAF50
        )

        self.add_stage_embeds(em, stages, reply_prefix)

        sent_message: discord.Message = await message.channel.send(embed=em)

        def reply_check(msg: discord.Message):
            return msg.content.startswith(bot.command_prefix) and \
                   msg.author == message.author and \
                   msg.channel == message.channel

        listen_for_replies = True
        start_index = 0

        while listen_for_replies:
            try:
                reply_message: discord.Message = await bot.wait_for(
                    "message",
                    timeout=120,
                    check=reply_check
                )

                if reply_message is not None:
                    # If we don't start with the reply prefix, but do start with the regular prefix, stop listening for
                    # replies
                    if reply_message.content.startswith(reply_prefix):
                        try:
                            await reply_message.delete()
                        except discord.Forbidden:
                            logging.info("Failed to delete reply message")

                        if reply_message.content.startswith(reply_prefix + "n"):  # Next page
                            em.clear_fields()
                            start_index += 10
                            self.add_stage_embeds(em, stages, reply_prefix, start_index)
                            await sent_message.edit(embed=em)

                        elif reply_message.content.startswith(reply_prefix + "p"):  # Prev page
                            em.clear_fields()
                            start_index -= 10
                            self.add_stage_embeds(em, stages, reply_prefix, start_index)
                            await sent_message.edit(embed=em)

                        elif reply_message.content.startswith(reply_prefix):  # Prev page
                            # Assume we want to get into about a stage
                            ret = self.int_try_parse(reply_message.content[2:])
                            if ret[1]:  # Check if parsing succeeded
                                listen_for_replies = False
                                em.clear_fields()

                                stage = stages[ret[0] - 1]

                                if stage.creator is None:
                                    creator = "Unknown"
                                else:
                                    creator = stage.creator.name

                                description = stage.description
                                if description is None or description == "":
                                    description = "No description provided"

                                embed_value = "**Creator**\n%s\n\n**Description**\n%s" % (creator, description)

                                em.title = "🏷️ " + stage.name
                                em.description = embed_value
                                em.colour = 0x2196F3
                                await sent_message.edit(embed=em)

                    else:
                        listen_for_replies = False
                        em.description = "Stopped listening for replies\nReason: Another bot command was performed"
                        em.colour = 0x2196F3
                        await sent_message.edit(embed=em)

            except asyncio.TimeoutError:
                listen_for_replies = False
                em.description = "Stopped listening for replies\nReason: Timed out"
                em.colour = 0x2196F3
                await sent_message.edit(embed=em)

    @classmethod
    def get_command_name(cls):
        return "find-stage"

    @classmethod
    def get_summary_text(cls):
        return "Searches for a Super Monkey Ball stage"

    @classmethod
    def get_help_text(cls):
        return "Searches for a Super Monkey Ball stage\n" \
               "**Usage**: %s name..." % cls.get_command_name()

    @staticmethod
    def add_stage_embeds(em: discord.Embed, stages: List[SmbStage], reply_prefix: str, start_index: int = 0):
        i = start_index
        for stage in stages[start_index:]:
            if stage.creator is None:
                creator = "Unknown"
            else:
                creator = stage.creator.name

            description = stage.description
            if description is None or description == "":
                description = "No description provided"

            embed_value = "**Creator**: %s\n%s" % (creator, description)

            em.add_field(name="__`%s%d` %s__" % (reply_prefix, i + 1, stage.name), value=embed_value, inline=False)
            i += 1

            if i == start_index + 10:
                break

    @staticmethod
    def int_try_parse(value):
        try:
            return int(value), True
        except ValueError:
            return value, False

