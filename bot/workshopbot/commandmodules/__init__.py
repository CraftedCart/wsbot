from .development import *
from .system import *
from .permissions import *
from .smb import *
from .replay import *

commandmodule_modules = [
    development,
    system,
    permissions,
    smb,
    replay
]
