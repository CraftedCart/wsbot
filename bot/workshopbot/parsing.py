from typing import List

import parsimonious


def parse(cmdline: str):
    grammar = parsimonious.Grammar(
        r"""
    cmdline = (command? ";"? space?)+
    command = ((double_quoted_arg / single_quoted_arg / unquoted_arg) space?)+
    unquoted_arg = ~"[^\s;]+"
    single_quoted_arg = ~"'[^']*'"
    double_quoted_arg = ~"\"[^\"]*\""
    space = ~"\s+"
    """)

    return grammar.parse(cmdline)


class Transform(parsimonious.NodeVisitor):
    def __init__(self):
        self.cmd_line = CmdLine()

    def visit_command(self, node, children):
        args = []

        for arg_group in node.children:
            for arg_subgroup in arg_group.children:
                for child in arg_subgroup.children:
                    if child.expr.name == "unquoted_arg":
                        args.append(child.full_text[child.start:child.end])
                    elif child.expr.name.endswith("_quoted_arg"):
                        args.append(child.full_text[child.start + 1:child.end - 1])

        self.cmd_line.commands.append(UserCommand(args))

        return children

    def generic_visit(self, node, children):
        return children


class CmdLine:
    def __init__(self):
        self.commands = []

    def __str__(self):
        str_commands = []
        for cmd in self.commands:
            str_commands.append(str(cmd))

        return "<%s>\n    %s" % (self.__class__.__name__, "\n    ".join(str_commands))


class UserCommand:
    def __init__(self, args: List[str]):
        self.argv = args

    def __str__(self):
        return "<%s>\n        %s\n" % (self.__class__.__name__, "\n        ".join(self.argv))

    @property
    def argc(self):
        return len(self.argv)
