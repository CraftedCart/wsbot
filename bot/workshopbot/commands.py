from typing import List

import discord

from workshopbot.bot import WorkshopBot
from workshopbot.parsing import UserCommand


class ICommand:
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        raise NotImplementedError(
            "Class %s doesn't implement execute(self, bot, message, user_cmd)" % self.__class__.__name__)

    @classmethod
    def get_command_name(cls):
        raise NotImplementedError("Class %s doesn't implement get_command_name(cls)" % cls.__name__)

    @classmethod
    def get_summary_text(cls):
        return ""

    @classmethod
    def get_help_text(cls):
        return "No help provided"

    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        """
        Returns a list of global group names permitted to run the command
        This overrides blacklisted global groups
        """
        return []

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        """
        Returns a list of global group names denied from running the command
        This can also contain "@everyone" to deny all but whitelisted global groups
        """
        return []
