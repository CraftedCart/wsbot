import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
Session: sqlalchemy.orm.sessionmaker = None

engine: sqlalchemy.engine.Engine = None
connection: sqlalchemy.engine.Connection = None
