import sqlite3
import atexit
from uuid import uuid4

conn: sqlite3.Connection = None


def init():
    global conn

    conn = sqlite3.connect("bot.sqlite")
    atexit.register(destroy)

    # Create permissions tables
    c = conn.cursor()
    c.execute("""
    CREATE TABLE IF NOT EXISTS guild_permission_groups (
        permission_uuid BLOB PRIMARY KEY UNIQUE, -- UUID
        guild_id INT8,
        permission_name TEXT -- Some human readable name
    )
    """)

    c.execute("""
    CREATE TABLE IF NOT EXISTS global_permission_groups (
        permission_uuid BLOB PRIMARY KEY UNIQUE, -- UUID
        permission_name TEXT -- Some human readable name
    )
    """)

    c.execute("""
    CREATE TABLE IF NOT EXISTS guild_user_permissions (
        user_id INT8,
        permission_uuid BLOB, -- UUID

        FOREIGN KEY(permission_uuid) REFERENCES guild_permission_groups(permission_uuid),
        UNIQUE(user_id, permission_uuid)
    )
    """)

    c.execute("""
    CREATE TABLE IF NOT EXISTS global_user_permissions (
        user_id INT8,
        permission_uuid BLOB, -- UUID

        FOREIGN KEY(permission_uuid) REFERENCES global_permission_groups(permission_uuid),
        UNIQUE(user_id, permission_uuid)
    )
    """)

    # Create default values
    # Check if the developer global group exists already
    c.execute("SELECT permission_uuid FROM global_permission_groups WHERE permission_name = \"developer\"")
    developer_uuid = c.fetchone()

    if developer_uuid is None:
        # It doesn't exist - create it and set defaults
        developer_uuid = uuid4()
        c.execute("""
        INSERT INTO global_permission_groups (
            permission_uuid,
            permission_name
        ) VALUES (
            ?,
            ?
        )
        """, (
            developer_uuid.bytes,
            "developer"
        ))

        c.execute("""
        INSERT INTO global_user_permissions (
            user_id,
            permission_uuid
        ) VALUES (
            ?,
            ?
        )
        """, (
            108875959628795904,  # CraftedCart
            developer_uuid.bytes
        ))

    conn.commit()

    # Create SMB tables
    c.executescript("""
    CREATE TABLE IF NOT EXISTS smb_creators (
        uuid BINARY,
        creator_name TEXT,
        creator_discord_id INTEGER
    );

    CREATE TABLE IF NOT EXISTS smb_stages (
        uuid BINARY,
        stage_name TEXT,
        stage_creator_uuid BINARY,
        stage_description TEXT,
        stage_image_url TEXT,

        FOREIGN KEY(stage_creator_uuid) REFERENCES smb_creators(uuid)
    );

    CREATE TABLE IF NOT EXISTS smb_packs (
        uuid BINARY,
        pack_name TEXT,
        pack_creator_uuid BINARY,

        FOREIGN KEY(pack_creator_uuid) REFERENCES smb_creators(uuid)
    );

    CREATE TABLE IF NOT EXISTS smb_pack_stages (
        pack_uuid BINARY,
        stage_uuid BINARY,
        stage_slot INTEGER,
        challenge_difficulty INTEGER,
        challenge_index INTEGER,
        story_index INTEGER,

        FOREIGN KEY(pack_uuid) REFERENCES smb_packs(uuid),
        FOREIGN KEY(stage_uuid) REFERENCES smb_stages(uuid)
    );
    """)


def commit():
    conn.commit()


def destroy():
    conn.close()


init()
