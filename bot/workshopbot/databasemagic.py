import logging
import sys
import time

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker

import workshopbot.database_globals
from workshopbot import config
from workshopbot.database_globals import Base
from workshopbot.permissions.global_group import GlobalGroup


def init():
    workshopbot.database_globals.engine = create_engine(config.SQLALCHEMY_DATABASE_URI)

    connected = False
    connection_attempt = 0
    while not connected:
        try:
            workshopbot.database_globals.connection = workshopbot.database_globals.engine.connect()
            workshopbot.database_globals.Session = sessionmaker(bind=workshopbot.database_globals.engine)

            connected = True
        except OperationalError:
            if connection_attempt < 10:
                logging.warning("Failed to connect to the database - assuming database is still starting")
                logging.warning("Will try again in 10 seconds [Attempt %d / 10]" % (connection_attempt + 1,))
                time.sleep(10)
            else:
                logging.exception("Failed to connect to the database after 10 attempts - Giving up now")
                sys.exit(1)

    logging.info("Connected to the database")
    Base.metadata.create_all(workshopbot.database_globals.engine)

    # Add default developers
    session = workshopbot.database_globals.Session()

    for group in config.DEFAULT_GLOBAL_GROUPS:
        existing_group = session.query(GlobalGroup).filter(GlobalGroup.uuid == group.uuid).first()

        if existing_group:
            logging.info("Default group %s already exists" % repr(existing_group))
        else:
            session.add(group)
            logging.info("Added default group %s" % repr(group))

    session.commit()
