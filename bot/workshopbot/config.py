from typing import List
from uuid import UUID

# SQLAlchemy
from workshopbot.permissions.global_group import GlobalGroup
from workshopbot.permissions.global_group_user import GlobalGroupUser

POSTGRES_USER = 'workshopbot'
POSTGRES_PASSWORD = 'workshopbot'
POSTGRES_DB = 'workshopbot-db'
SQLALCHEMY_DATABASE_URI = \
    'postgresql+psycopg2://' + POSTGRES_USER + ':' + POSTGRES_PASSWORD + '@postgres:5001/' + POSTGRES_DB

# Permission groups
craftedcart_user = GlobalGroupUser(
    user_snowflake=108875959628795904
)

developer_group = GlobalGroup(
    uuid=UUID("{f24639f8-4a19-4aba-851f-0b552a7eb697}"),
    name="developer",
    users=[craftedcart_user]
)

DEFAULT_GLOBAL_GROUPS: List[GlobalGroup] = [
    developer_group
]
