from typing import List, Optional
from uuid import UUID, uuid4

from workshopbot import database


class PermissionGroup:
    def __init__(self, permission_uuid: UUID, permission_name: str):
        self.permission_uuid = permission_uuid
        self.permission_name = permission_name


def add_permission_group(guild_id: int, name: str) -> UUID:
    uuid = uuid4()

    c = database.conn.cursor()
    c.execute("""
    INSERT INTO guild_permission_groups (
        guild_id,
        permission_uuid,
        permission_name
    ) VALUES (
        ?,
        ?,
        ?
    )
    """, (
        guild_id,
        uuid.bytes,
        name
    ))
    database.conn.commit()

    return uuid


def get_user_global_permissions(user_id: int) -> List[PermissionGroup]:
    out = []

    c = database.conn.cursor()
    for row in c.execute("""
    SELECT
        global_permission_groups.permission_uuid, global_permission_groups.permission_name
    FROM
        global_user_permissions LEFT JOIN global_permission_groups
        ON
            global_user_permissions.permission_uuid = global_permission_groups.permission_uuid
    WHERE
        user_id = ?
    """, (
            user_id,
    )):
        out.append(PermissionGroup(UUID(bytes=row[0]), row[1]))

    return out


def get_user_guild_permissions(user_id: int, guild_id: int) -> List[PermissionGroup]:
    out = []

    c = database.conn.cursor()
    for row in c.execute("""
    SELECT
        guild_permission_groups.permission_uuid, guild_permission_groups.permission_name
    FROM
        guild_user_permissions LEFT JOIN guild_permission_groups
        ON
            guild_user_permissions.permission_uuid = guild_permission_groups.permission_uuid
    WHERE
        user_id = ? AND
        guild_id = ?
    """, (
            user_id, guild_id
    )):
        out.append(PermissionGroup(UUID(bytes=row[0]), row[1]))

    return out


def get_guild_group_uuid_from_name(guild_id: int, permission_name: str) -> Optional[UUID]:
    c = database.conn.cursor()
    c.execute("""
    SELECT
        permission_uuid
    FROM
        guild_permission_groups
    WHERE
        guild_id = ? AND
        permission_name = ?
    """, (
        guild_id, permission_name
    ))

    result = c.fetchone()

    if result is None:
        return None
    else:
        return UUID(bytes=result[0])


def add_user_to_guild_group(user_id: int, permission_uuid: UUID):
    c = database.conn.cursor()
    c.execute("""
        INSERT INTO guild_user_permissions (
            user_id,
            permission_uuid
        ) VALUES (
            ?,
            ?
        )
        """, (
        user_id, permission_uuid.bytes
    ))

    database.conn.commit()


def is_user_in_global_group(user_id: int, permission_name: str) -> bool:
    c = database.conn.cursor()
    c.execute("""
    SELECT
        global_permission_groups.permission_uuid, global_permission_groups.permission_name
    FROM
        global_user_permissions LEFT JOIN global_permission_groups
        ON
            global_user_permissions.permission_uuid = global_permission_groups.permission_uuid
    WHERE
        user_id = ? AND
        permission_name = ?
    """, (
            user_id, permission_name
    ))

    result = c.fetchone()

    return result is not None


def get_global_permission_groups():
    out = []

    c = database.conn.cursor()
    for row in c.execute("""
    SELECT
        global_permission_groups.permission_uuid, global_permission_groups.permission_name
    FROM
        global_permission_groups
    """):
        out.append(PermissionGroup(UUID(bytes=row[0]), row[1]))

    return out


def get_guild_permission_groups(guild_id: int):
    out = []

    c = database.conn.cursor()
    for row in c.execute("""
    SELECT
        guild_permission_groups.permission_uuid, guild_permission_groups.permission_name
    FROM
        guild_permission_groups
    WHERE
        guild_id = ?
    """, (
            guild_id,
    )):
        out.append(PermissionGroup(UUID(bytes=row[0]), row[1]))

    return out


def del_permission_group(guild_id: int, permission_name: str) -> bool:
    c = database.conn.cursor()
    # Delete users from the group
    c.execute("""
    DELETE FROM
        guild_user_permissions
    WHERE
        permission_uuid IN (
            SELECT
                permission_uuid
            FROM
                guild_permission_groups
            WHERE
                permission_name = ? AND
                guild_id = ?
        )
    """, (
        permission_name,
        guild_id
    ))

    # Delete the group itself
    c.execute("""
    DELETE FROM
        guild_permission_groups
    WHERE
        permission_name = ? AND
        guild_id = ?
    """, (
        permission_name,
        guild_id
    ))
    database.conn.commit()

    return c.rowcount != 0  # If 0 rows are affected, this failed, otherwise we succeeded
