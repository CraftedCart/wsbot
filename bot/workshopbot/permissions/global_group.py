from uuid import uuid4

from sqlalchemy import Column, String
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import relationship

from workshopbot.database_globals import Base


class GlobalGroup(Base):
    __tablename__ = "global_groups"

    uuid = Column(postgresql.UUID(as_uuid=True), unique=True, nullable=False, primary_key=True, default=uuid4)
    name = Column(String(64), unique=True, nullable=False)

    users = relationship("GlobalGroupUser", backref="global_group", lazy="dynamic")

    def __repr__(self):
        return "<GlobalPermissionGroup (uuid=%s, name='%s'>" % (str(self.uuid), self.name)
