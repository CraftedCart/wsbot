from uuid import uuid4

from sqlalchemy import Column, ForeignKey, BigInteger
from sqlalchemy.dialects import postgresql

from workshopbot.database_globals import Base


class GlobalGroupUser(Base):
    __tablename__ = "global_group_users"

    uuid = Column(postgresql.UUID(as_uuid=True), unique=True, nullable=False, primary_key=True, default=uuid4)
    group_uuid = Column(postgresql.UUID(as_uuid=True), ForeignKey("global_groups.uuid"), nullable=False)
    user_snowflake = Column(BigInteger, unique=True, nullable=False)

    def __repr__(self):
        return "<GlobalGroupUser (uuid=%s, group_uuid=%s user_snowflake=%d>" % \
               (str(self.uuid), str(self.group_uuid), self.user_snowflake)
