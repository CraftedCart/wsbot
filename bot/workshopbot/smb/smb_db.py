from typing import Optional, List
from uuid import UUID, uuid4

from workshopbot import database
from workshopbot.smb.smb_creator import SmbCreator
from workshopbot.smb.smb_stage import SmbStage


def add_creator(creator_name: str, creator_discord_id: Optional[int] = None) -> UUID:
    uuid = uuid4()

    c = database.conn.cursor()
    c.execute("""
    INSERT INTO smb_creators (
        uuid,
        creator_name,
        creator_discord_id
    ) VALUES (
        ?,
        ?,
        ?
    )
    """, (
        uuid.bytes,
        creator_name,
        creator_discord_id
    ))
    database.conn.commit()

    return uuid


def add_stage(
        stage_name: str,
        creator_uuid: UUID,
        stage_description: Optional[str],
        stage_image_url: [Optional[str]]) -> UUID:
    uuid = uuid4()

    c = database.conn.cursor()
    c.execute("""
    INSERT INTO smb_stages (
        uuid,
        stage_name,
        stage_creator_uuid,
        stage_description,
        stage_image_url
    ) VALUES (
        ?,
        ?,
        ?,
        ?,
        ?
    )
    """, (
        uuid.bytes,
        stage_name,
        creator_uuid.bytes,
        stage_description,
        stage_image_url
    ))
    database.conn.commit()

    return uuid


def add_stage_batched(
        stage_name: str,
        creator_uuid: UUID,
        stage_description: Optional[str],
        stage_image_url: [Optional[str]]) -> UUID:
    """
    Like add_stage but doesn't commit
    """

    uuid = uuid4()

    c = database.conn.cursor()
    c.execute("""
    INSERT INTO smb_stages (
        uuid,
        stage_name,
        stage_creator_uuid,
        stage_description,
        stage_image_url
    ) VALUES (
        ?,
        ?,
        ?,
        ?,
        ?
    )
    """, (
        uuid.bytes,
        stage_name,
        creator_uuid.bytes,
        stage_description,
        stage_image_url
    ))

    return uuid


def find_stages_by_name(name: str) -> List[SmbStage]:
    fuzzy_name = name.replace(" ", "%")
    # fuzzy_name = "%".join(fuzzy_name)
    fuzzy_name = "%" + fuzzy_name + "%"

    out = []

    c = database.conn.cursor()
    for row in c.execute("""
    SELECT
        stage_name,
        stage_description,
        creator_name,
        creator_discord_id
    FROM
        smb_stages LEFT JOIN smb_creators
    WHERE
        stage_name COLLATE NOCASE LIKE ?
    """, (
        fuzzy_name,
    )):
        creator = SmbCreator(name=row[2], discord_id=row[3])
        stage = SmbStage(name=row[0], description=row[1], creator=creator)
        out.append(stage)

    return out
